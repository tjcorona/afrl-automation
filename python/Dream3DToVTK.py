#!/usr/bin/env python

""" Dream3DToVTK.py:

For the AFRL Materials Phase II Demo, takes the output of DREAM3D and converts
the mesh describing ellipsoids into a VTK file.

Data structures extracted from DREAM3D:
  - 2-D lattice
  - points describing the different zones

"""

import vtk


def dream3DToVTK(inputFile, outputFile):
    # DREAM3D output Xdmf file
    xdmfReader = vtk.vtkXdmfReader()
    xdmfReader.SetFileName(inputFile)
#    xdmfReader.Update()
    return xdmfReader

if __name__ == "__main__":

    import argparse
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help='<Required> Input DREAM3D xmdf file',
                        required=True)

    parser.add_argument('-m', '--mesh-file',
                        help='Output mesh vtu file',
                        default="")
    args = parser.parse_args()

    if args.mesh_file == "":
        args.mesh_file = os.path.splitext(args.input_file)[0] + '_mesh.vti'

    dream3DToVTK(args.input_file, args.mesh_file)
