#!/usr/bin/env python

""" ContiguousDomains.py:

For the AFRL Materials Phase II Demo, adds cell data "domain" that identifies
each domain with an integral value. The resulting range of this data is
contiguous and starts at 0.

"""

import vtk


def contiguousDomains(pointToCell, activeScalars, saveFile):
    pointToCell.Update()

    mesh = pointToCell.GetOutput()

    mesh.GetCellData().SetActiveScalars(activeScalars)
    data = mesh.GetCellData().GetScalars()

    hist = [0] * int(data.GetRange()[1] - data.GetRange()[0] + 1)

    for i in xrange(data.GetNumberOfTuples()):
        index = int(data.GetValue(i))
        hist[index] = hist[index] + 1

    mapping = [-1] * len(hist)

    newIndex = 0
    for i in xrange(len(hist)):
        if hist[i] > 0:
            mapping[i] = newIndex
            newIndex = newIndex + 1

    domain = vtk.vtkIntArray()
    domainName = "Domain"
    domain.SetName(domainName)
    domain.SetNumberOfComponents(1)
    domain.SetNumberOfTuples(mesh.GetNumberOfCells())
    mesh.GetCellData().AddArray(domain)

    for i in xrange(data.GetNumberOfTuples()):
        domain.SetValue(i, mapping[int(data.GetValue(i))])

    mesh.GetCellData().SetActiveScalars(domainName)

    xmlWriter = vtk.vtkXMLUnstructuredGridWriter()
    xmlWriter.SetFileName(saveFile)
    xmlWriter.SetInputData(mesh)
    xmlWriter.Write()


if __name__ == '__main__':

    import argparse
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help=('<Required> Input mesh vtu file with '
                              'classification data (turbine blade)'),
                        required=True)

    parser.add_argument('-a', '--active-scalars',
                        help='name of the active scalars data',
                        default="FeatureIds")

    parser.add_argument('-s', '--save-file',
                        help=('Output mesh vtu file with contiguous '
                              'classification data'),
                        default="")

    args = parser.parse_args()

    if args.save_file == "":
        args.save_file = args.input_file

    contiguousDomains(args.input_file, args.active_scalars, args.save_file)
