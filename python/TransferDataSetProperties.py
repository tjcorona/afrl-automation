#!/usr/bin/env python

""" TransferDataSetProperties.py:

For the AFRL Materials Phase II Demo, transforms the mesh with ellipsoids onto
the ROI of the turbine blade and applies its cell data onto the the blade, and
writes the new dataset to file.

"""

import os.path
import random
import vtk


def createRotationFilter(vtkDataSet, rotate):
    # First, let's center the cube before rotation
    bounds = vtkDataSet.GetBounds()
    transX = -1.0 * (bounds[1] - bounds[0]) / 2.0
    transY = -1.0 * (bounds[3] - bounds[2]) / 2.0
    transZ = -1.0 * (bounds[5] - bounds[4]) / 2.0
    #print 'transX, transY, and transZ are', transX, transY, transZ

    # Center the cube
    rTransform1 = vtk.vtkTransform()
    rTransform1.Translate(transX, transY, transZ)

    # Rotate it
    rTransform2 = vtk.vtkTransform()
    rTransform2.RotateX(rotate[0])
    rTransform2.RotateY(rotate[1])
    rTransform2.RotateZ(rotate[2])
    rTransform2.Concatenate(rTransform1)

    # Move it back to where it was
    rTransform3 = vtk.vtkTransform()
    rTransform3.Translate(-transX, -transY, -transZ)
    rTransform3.Concatenate(rTransform2)

    rTransformFilter = vtk.vtkTransformFilter()
    rTransformFilter.SetTransform(rTransform3)

    return rTransformFilter


# A single file may be passed to "originalFileList". If that is the case,
# then translateList, scaleList, and rotateList should not be lists of the
# respected operation either.
# If "originalFileList" is a list, though, translateList, scaleList, and
# rotateList must be lists that have the same numbers of elements. This
# allows us to use multiple cubes.
def transferDataSetProperties(threshold, xdmfReaderList, activeScalars,
                              saveFile, translateList, scaleList, rotateList):

    if not isinstance(xdmfReaderList, list):
        xdmfReaderList = [xdmfReaderList]
        translateList = [translateList]
        scaleList = [scaleList]
        rotateList = [rotateList]

    numCubes = len(xdmfReaderList)
    assert(isinstance(translateList, list) and len(translateList) == numCubes)
    assert(isinstance(scaleList, list) and len(scaleList) == numCubes)
    assert(isinstance(rotateList, list) and len(rotateList) == numCubes)

    offset = 0
    appendFilter = vtk.vtkAppendFilter()
    for i in range(len(xdmfReaderList)):

        # This needs to be updated so that createRotationFilter() will know
        # the bounds of the cell
        xdmfReaderList[i].Update()

        # This will center the cube, rotate it, and then move it back to where
        # it was originally.
        rotateTransformFilter = createRotationFilter(
            xdmfReaderList[i].GetOutputDataObject(0), rotateList[i])
        rotateTransformFilter.SetInputConnection(
            xdmfReaderList[i].GetOutputPort())

        transform = vtk.vtkTransform()
        transform.Translate(translateList[i])
        transform.Scale([scaleList[i]] * 3)

        transformFilter = vtk.vtkTransformFilter()
        transformFilter.SetTransform(transform)
        transformFilter.SetInputConnection(
            rotateTransformFilter.GetOutputPort())

        outputDir = os.path.dirname(saveFile)
        dream3dFinalFileName = "dream3d_cube_final" + str(i) + ".vts"
        dream3dFinalFile = outputDir + "/" + dream3dFinalFileName
        # Show the final form of the cube before transfering properties
        xmlWriter = vtk.vtkXMLStructuredGridWriter()
        xmlWriter.SetFileName(dream3dFinalFile)
        transformFilter.Update()
        xmlWriter.SetInputData(transformFilter.GetOutputDataObject(0))
        xmlWriter.Write()

        mesh = transformFilter.GetOutput()
        mesh.GetCellData().SetActiveScalars(activeScalars)
        data = mesh.GetCellData().GetScalars()
        #print 'range was', data.GetRange()
        if offset != 0:
            for j in xrange(data.GetNumberOfTuples()):
                value = int(data.GetValue(j))
                # 1 is the value of the base cube. We will leave that alone
                # and only offset the other values
                if value > 1:
                    data.SetValue(j, value + offset)
            data.Modified()
        #print 'range is now', data.GetRange()
        # We leave 1 alone and offset the rest of the values. So subtract
        # 1 from the range.
        offset = int(data.GetRange()[1] - 1)
        #print 'new offset is', offset

        appendFilter.AddInputData(transformFilter.GetOutput())

    probeFilter = vtk.vtkProbeFilter()
    probeFilter.SetInputConnection(threshold.GetOutputPort())
    probeFilter.SetSourceConnection(appendFilter.GetOutputPort())
    probeFilter.Update()

    # Set the ObjectId to interpolate via nearest neighbor. That way, we won't
    # get any false values (for example, ellipsoid 4 classification appearing
    # on the boundary of ellipsoids 3 and 5).
    grid = probeFilter.GetOutput()
    index = grid.GetPointData().SetActiveScalars(activeScalars)
    dsAtts = vtk.vtkDataSetAttributes()
    if index != -1:
        grid.GetPointData().SetCopyAttribute(dsAtts.SCALARS, 2,
                                             dsAtts.INTERPOLATE)

    # Cells that are outside of the region of classification are given the
    # value 0.
    # Cells that are in the region of classification but are not in an
    # ellipsoid are given the value 1.
    # For all values that were adopted via classification, let's reduce their
    # classification index by 1.
    data = grid.GetPointData().GetScalars()
    for i in xrange(data.GetNumberOfTuples()):
        value = int(data.GetValue(i))
        if value > 0:
            data.SetValue(i, value - 1)

    # Re-assign hard zones to 1 and soft zones to 2
    for i in xrange(data.GetNumberOfTuples()):
        value = int(data.GetValue(i))
        if value > 0:
            hard = bool(random.getrandbits(1))
            if hard:
                data.SetValue(i, 1)
            else:
                data.SetValue(i, 2)

    pointToCell = vtk.vtkPointDataToCellData()
    pointToCell.SetInputConnection(probeFilter.GetOutputPort())
    pointToCell.SetCategoricalData(True)

    return pointToCell

#    xmlWriter = vtk.vtkXMLUnstructuredGridWriter()
#    xmlWriter.SetFileName(saveFile)
#    xmlWriter.SetInputConnection(pointToCell.GetOutputPort())
#    xmlWriter.Write()


if __name__ == "__main__":

    import argparse
    import os
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help=('<Required> Input mesh vtu file that '
                              'takes the data (turbine blade)'),
                        required=True)

    parser.add_argument('-o', '--original-file',
                        help=('<Required> Original mesh vti file containing '
                              'the data (ellipsoidal mesh)'),
                        required=True)

    parser.add_argument('-a', '--active-scalars',
                        help='name of the active scalars data',
                        default="FeatureIds")

    parser.add_argument('-s', '--save-file',
                        help='Output mesh vtu file',
                        default="")

    parser.add_argument('-r', '--rotate', nargs=3, type=float,
                        help=('Rotate the ellipsoidal mesh by x, then y, then '
                              'z. The cube is centered before rotation and '
                              'then translated back to where it was after '
                              'rotation, so that the rotation is independent '
                              'of the translation and scaling'),
                        default=[0.0, 0.0, 0.0])

    parser.add_argument('-t', '--translate', nargs=3, type=float,
                        help='translate the ellipsoidal mesh',
                        default=[-7, .5, 47.5])

    parser.add_argument('-c', '--scale', type=float,
                        help='scale the ellipsoidal mesh',
                        default=0.0035)

    args = parser.parse_args()

    if args.save_file == "":
        args.save_file = os.path.splitext(args.input_file)[
            0] + '_classified.vtu'

    transferDataSetProperties(args.input_file, args.original_file,
                              args.active_scalars, args.save_file,
                              args.translate, args.scale, args.rotate)
