#!/usr/bin/env python

""" ApplyElastoMechanicalBoundaries_TestBar.py:

"""

import math
import random
import smtk
import smtk.io
import smtk.io.vtk
import smtk.mesh
import sys
import vtk


class EdgeFilter(smtk.mesh.CellForEach):
    def __init__(self, normal):
        smtk.mesh.CellForEach.__init__(self, True)
        self.validPoints = list()
        self.normal = normal

    def forCell(self, meshHandle, cellType, numPoints):
        if numPoints < 3:
            return

        # unit vector from first point to second point in cell
        v1 = [0., 0., 0.]
        # unit vector from first point to third point in cell
        v2 = [0., 0., 0.]
        # unit normal of cell
        normal = [0., 0., 0.]

        # compute v0,v1,v2
        for i in range(0, 3):
            v1[i] = self.coordinates()[3+i] - self.coordinates()[i]
            v2[i] = self.coordinates()[6+i] - self.coordinates()[i]

        # compute normal
        for i in range(0, 3):
            i1 = (i+1) % 3
            i2 = (i+2) % 3
            normal[i] = v1[i1]*v2[i2] - v1[i2]*v2[i1]

        mag = math.sqrt(normal[0]*normal[0] +
                        normal[1]*normal[1] +
                        normal[2]*normal[2])

        normal = map(lambda x: x/mag, normal)

        dot = normal[0]*self.normal[0] + normal[1] * \
            self.normal[1] + normal[2]*self.normal[2]

        if dot < .999:
            return

        for i in xrange(0, numPoints):
            self.validPoints.append(self.pointId(i))


def labelIntersection(c, shell, filter_):
    shellCells = shell.cells()

    smtk.mesh.for_each(shellCells, filter_)
    filteredCells = smtk.mesh.CellSet(c, filter_.validPoints)

    domains = c.domains()

    for dom in domains:
        domainMeshes = c.meshes(dom)

        domainCells = domainMeshes.cells()
        contactCells = smtk.mesh.point_intersect(
            domainCells, filteredCells, smtk.mesh.FullyContained)

        if not contactCells.is_empty():
            contactD = c.createMesh(contactCells)
            c.setDirichletOnMeshes(contactD,
                                   smtk.mesh.Dirichlet(
                                       labelIntersection.nextDirId))
            labelIntersection.nextDirId += 1

    return True


labelIntersection.nextDirId = 0


def breakMaterialsByCellType(c, randomizeDomainVals):
    domains = c.domains()

    domainVals = []
    if randomizeDomainVals:
        for i in range(len(domains)):
            domainVals.append(i)

        random.shuffle(domainVals)
    else:
        for dom in domains:
            domainVals.append(dom.value())

    domainsMade = 0
    for i, dom in enumerate(domains):
        domainMeshes = c.meshes(dom)

        for ct in xrange(smtk.mesh.Line, smtk.mesh.CellType_MAX):
            cells = domainMeshes.cells(smtk.mesh.CellType(ct))
            if not cells.is_empty():
                ms = c.createMesh(cells)
                v = (domainVals[i] * 100) + ct
                c.setDomainOnMeshes(ms, smtk.mesh.Domain(v))
                domainsMade += 1

        c.removeMeshes(domainMeshes)


def convert(inputFile, manager, material):
    cnvrt = smtk.io.vtk.ImportVTKData()
    collection = cnvrt(inputFile, manager, material)
    return collection


def extractMaterials(c, outputFile, randomizeDomainVals):
    shell = c.meshes().extractShell()
    print ('There are %d shell mesh sets' % shell.size())

    filter_ = EdgeFilter([1., 0., 0.])
    labelIntersection(c, shell, filter_)

    filter_ = EdgeFilter([-1., 0., 0.])
    labelIntersection(c, shell, filter_)

    breakMaterialsByCellType(c, randomizeDomainVals)

    print ('number of domains: %d' % len(c.domains()))
    print ('number of dirichlets: %d' % len(c.dirichlets()))

    smtk.io.writeEntireCollection(outputFile, c)


def applyElastomechanicalBoundaries_TestBar(inputFile, saveName,
                                            domainName='Domain',
                                            randomizeDomainVals=False):
    manager = smtk.mesh.Manager.create()

    c = convert(inputFile, manager, domainName)
    if c is None:
        raise IOError('Could not convert input file into a mesh')

    extractMaterials(c, saveName, randomizeDomainVals)


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help='<Required> Input data',
                        required=True)

    parser.add_argument('-s', '--save-name',
                        help='name of output Exodus II file',
                        default='out.exo')

    parser.add_argument('-d', '--domain-name',
                        help='name of the domain cell categorical data',
                        default='Domain')

    parser.add_argument('-r', '--randomize-domain-vals',
                        help='Randomize the domain values?', type=bool,
                        default=False)

    args = parser.parse_args()

    applyElastomechanicalBoundaries_TestBar(args.input_file, args.save_name,
                                            args.domain_name,
                                            args.randomize_domain_vals)
