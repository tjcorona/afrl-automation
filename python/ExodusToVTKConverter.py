#!/usr/bin/env python

""" ExodusToVTKConverter.py:

For the AFRL Materials Phase II Demo, takes an Exodus II file and converts the
mesh into a VTK file. Also, cells that are not tetrahedra are removed from the
mesh.

Data structures extracted from Exodus II:
  - 3-D tetrahedral mesh

"""

import vtk


def exodusToVTKConverter(inputFile, outputFile, blockName=''):
    exodusReader = vtk.vtkExodusIIReader()
    exodusReader.SetFileName(inputFile)
    exodusReader.SetElementResultArrayStatus("FeatureIds", 1)
    exodusReader.Update()

    dataNames = [exodusReader.GetOutputDataObject(0).GetMetaData(i)
                 .Get(vtk.vtkCompositeDataSet.NAME()) for i in
                 xrange(exodusReader.GetOutputDataObject(0)
                        .GetNumberOfBlocks())]

    blockContainer = exodusReader.GetOutputDataObject(0).GetBlock(
        dataNames.index('Element Blocks'))

    dataNames = [blockContainer.GetMetaData(i)
                 .Get(vtk.vtkCompositeDataSet.NAME()) for i in
                 xrange(blockContainer.GetNumberOfBlocks())]

    if blockName != "":
        region = blockContainer.GetBlock(dataNames.index(blockName))
    else:
        append = vtk.vtkAppendFilter()
        append.MergePointsOn()
        for i in xrange(len(dataNames)):
            append.AddInputData(blockContainer.GetBlock(i))
        append.Update()
        region = append.GetOutputDataObject(0)

    cellTypes = vtk.vtkIntArray()
    cellTypes.SetName("cell types")
    cellTypes.SetNumberOfComponents(1)
    cellTypes.SetNumberOfTuples(region.GetNumberOfCells())
    region.GetCellData().AddArray(cellTypes)
    for i in xrange(region.GetNumberOfCells()):
        cellTypes.SetValue(i, region.GetCell(i).GetCellType())

    region.GetCellData().SetActiveScalars("cell types")

    threshold = vtk.vtkThreshold()
    threshold.SetInputData(region)
    threshold.ThresholdBetween(vtk.VTK_TETRA, vtk.VTK_TETRA)

    return threshold

    #xmlWriter = vtk.vtkXMLUnstructuredGridWriter()
    #xmlWriter.SetFileName(outputFile)
    #xmlWriter.SetInputConnection(threshold.GetOutputPort())
    #xmlWriter.Write()


if __name__ == "__main__":

    import argparse
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help='<Required> Input Exodus II file',
                        required=True)

    parser.add_argument('-b', '--block-name',
                        help='Name of the block to extract',
                        default="")

    parser.add_argument('-m', '--mesh-file',
                        help='Output mesh vtu file',
                        default="")
    args = parser.parse_args()

    if args.mesh_file == "":
        args.mesh_file = os.path.splitext(args.input_file)[0] + '_mesh.vtu'

    exodusToVTKConverter(args.input_file, args.mesh_file, args.block_name)
