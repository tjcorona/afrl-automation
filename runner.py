""" runner.py:

For the AFRL Materials Case II Demo, automates
all of the steps in the albany generation process.

"""

import argparse
import os
import json
import sys

# Add the python directory so we can import from it
rootDir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(rootDir + "/python")

from Dream3DToVTK import *
from ExodusToVTKConverter import *
from TransferDataSetProperties import *
from ContiguousDomains import *
from ApplyElastomechanicalBoundaries import applyElastomechanicalBoundaries
from ApplyElastomechanicalBoundaries_TestBar import applyElastomechanicalBoundaries_TestBar
from GenerateAlbanyInputs import *

parser = argparse.ArgumentParser()
parser.add_argument('inputFile',
                    help=('<Required> The json input file with all of the '
                          'settings'),
                    default='')

args = parser.parse_args()

inputFile = args.inputFile

if not inputFile:
    sys.exit('Usage: <script> <inputFile>')


def setDefaultOptions(settings):
    defaultsFile = rootDir + '/defaultSettings.json'
    with open(defaultsFile, 'r') as rf:
        defaults = json.load(rf)

    for key in defaults.keys():
        if key not in settings:
            settings[key] = defaults[key]

# Now read the input file
with open(inputFile, 'r') as rf:
    settings = json.load(rf)

# Set the default options
setDefaultOptions(settings)

workingDir = os.path.dirname(os.path.realpath(inputFile))
model = settings["model"].lower()
originalMeshFile = settings["originalMeshFile"]
albanyInputTemplateFile = settings["albanyInputTemplateFile"]
hardnessGenerationStyle = settings["hardnessGenerationStyle"]

usingMultipleCubes = False
if "enableMultipleCubes" in settings:
    usingMultipleCubes = settings["enableMultipleCubes"]

# We are not using multiple cubes
if not usingMultipleCubes:
    ellipsoidalMeshFile = settings["ellipsoidalMeshFile"]
    transferDataSetTranslation = settings["transferDataSetTranslation"]
    transferDataSetScale = settings["transferDataSetScale"]
    transferDataSetRotation = settings["transferDataSetRotation"]
else:
    ellipsoidalMeshFiles = settings["ellipsoidalMeshFiles"]
    transferDataSetTranslations = settings["transferDataSetTranslations"]
    transferDataSetScales = settings["transferDataSetScales"]
    transferDataSetRotations = settings["transferDataSetRotations"]

    # They must all be equal in size
    assert(len(ellipsoidalMeshFiles) == len(transferDataSetTranslations))
    assert(len(ellipsoidalMeshFiles) == len(transferDataSetScales))
    assert(len(ellipsoidalMeshFiles) == len(transferDataSetRotations))


if model == 'testbar':
    rootName = 'TestBar'
elif model == 'turbine':
    rootName = 'Turbine'
else:
    print 'Error: unknown model:', model
    sys.exit()

if not os.path.exists(workingDir):
    os.makedirs(workingDir)

# ExodusToVTKConverter
print 'Running ExodusToVTKConverter'
inputFile = originalMeshFile
outputFile = workingDir + '/' + rootName + '.vtu'
blockName = ''
threshold = exodusToVTKConverter(inputFile, outputFile, blockName)

originalMesh = outputFile

if not usingMultipleCubes:
    # Standard, one-cube system
    # Dream3DToVTK
    print 'Running Dream3DToVTK'
    inputFile = ellipsoidalMeshFile
    outputFile = workingDir + '/dream3d_cube_original.vti'
    xdmfReader = dream3DToVTK(inputFile, None)

    # TransferDataSetProperties
    print 'Running TransferDataSetProperties'
    inputFile = originalMesh
    originalFile = xdmfReader
    activeScalars = "FeatureIds"
    saveFile = workingDir + '/' + rootName + '_classified.vtu'
    translate = transferDataSetTranslation
    scale = transferDataSetScale
    rotate = transferDataSetRotation
    pointToCell = transferDataSetProperties(threshold, xdmfReader, activeScalars, saveFile,
                              translate, scale, rotate)
else:
    # Multi-cube system
    # Dream3DToVTK
    xdmfReaders = []
    for i in range(len(ellipsoidalMeshFiles)):
        inputFile = ellipsoidalMeshFiles[i]
        outputFile = workingDir + '/dream3d_cube_original' + str(i) + '.vti'
        xdmfReader = dream3DToVTK(inputFile, outputFile)

        xdmfReaders.append(xdmfReader)

    inputFile = originalMesh
    activeScalars = "FeatureIds"
    saveFile = workingDir + '/' + rootName + '_classified.vtu'
    translateList = transferDataSetTranslations
    scaleList = transferDataSetScales
    rotateList = transferDataSetRotations
    pointToCell = transferDataSetProperties(threshold, xdmfReaders, activeScalars,
                              saveFile, translateList, scaleList, rotateList)

classifiedMesh = saveFile

# ContiguousDomains
print 'Running ContiguousDomains'
inputFile = classifiedMesh
activeScalars = "FeatureIds"
saveFile = workingDir + '/' + rootName + '_contiguous.vtu'
contiguousDomains(pointToCell, activeScalars, saveFile)

contiguousMesh = saveFile

print 'Running ApplyElastoMechanicalBoundaries'
if model == 'testbar':
    # ApplyElastoMechanicalBoundaries_TestBar
    inputFile = contiguousMesh
    saveFile = workingDir + "/" + rootName + "_BC.exo"
    domainName = 'Domain'
    randomizeDomains = False
    if hardnessGenerationStyle == 'randomLabelled':
        randomizeDomains = True
    applyElastomechanicalBoundaries_TestBar(inputFile, saveFile, domainName,
                                            randomizeDomains)
    bcMesh = saveFile
elif model == 'turbine':
    # ApplyElastoMechanicalBoundaries
    inputFile = contiguousMesh
    saveFile = workingDir + "/" + rootName + "_BC.exo"
    domainName = 'Domain'
    radius = 30.0
    frontFaceRMax = 52.0
    innerEdgeRMax = 4.01
    sideFaceRMax = 53.0
    origin = [0.0, 0.0, 0.0]
    randomizeDomains = False
    if hardnessGenerationStyle == 'randomLabelled':
        randomizeDomains = True
    applyElastomechanicalBoundaries(inputFile, saveFile, domainName, radius,
                                    frontFaceRMax, innerEdgeRMax, sideFaceRMax,
                                    origin, randomizeDomains)
    bcMesh = saveFile
else:
    print 'Error: unknown model:', model
    sys.exit()


# GenerateAlbanyInputs
print 'Generating albany inputs'
inputFile = bcMesh
albanyFile = workingDir + '/input.yaml'
elasticFile = workingDir + '/elastic.yaml'
inputTemplate = albanyInputTemplateFile
albanyOutputName = 'output.exo'

# Generate the materials to be used
# This will use all default settings
materials = []
basemat = {}
basemat['materialName'] = 'basemat'
basemat['elasticModulusValue'] = settings['basematElasticModulus']
materials.append(basemat)

hardmat = {}
hardmat['materialName'] = 'hardmat'
hardmat['elasticModulusValue'] = settings['hardmatElasticModulus']
materials.append(hardmat)

softmat = {}
softmat['materialName'] = 'softmat'
softmat['elasticModulusValue'] = settings['softmatElasticModulus']
materials.append(softmat)

applyDefaultSettingsToMaterials(materials)

generateAlbanyInputs(inputFile, albanyFile, elasticFile, inputTemplate,
                     albanyOutputName, materials, hardnessGenerationStyle)

print 'Done!'
