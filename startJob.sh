#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: <script> <inputFile>"
    exit 1
fi

inputFile=$1
workingDir=$(dirname $inputFile)
containerName=$(basename $workingDir)
echo 'inputFile is ' $inputFile
echo 'workingDir is ' $workingDir

./dockerRun.sh $inputFile > $workingDir/job.out 2>&1 &
